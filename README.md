# GATE IE worker

Worker implementation for GATE-based IE applications.  This will be built into a base Docker image which can be inherited by child images to add a particular xgapp and associated metadata to produce a runnable image for the ELG infrastructure.