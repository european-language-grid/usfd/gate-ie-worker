/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.util

import gate.Document
import gate.Factory
import gate.corpora.RepositioningInfo
import gate.DocumentFormat
import gate.corpora.MimeType
import gate.elg.config.GateElgProperties
import gate.util.persistence.PersistenceManager
import gate.util.spring.Init
import gate.util.spring.SpringFactory
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired

import javax.annotation.PostConstruct
import javax.annotation.Resource

/**
 * Class to allow extension of the document format mime type mappings via
 * system properties.  This class is not actually a document format, but it
 * extends DocumentFormat to get access to the protected static maps defined
 * therein.
 */
@Slf4j
public class MimeTypeMapper extends DocumentFormat {

  GateElgProperties gateElgProperties

  // trick to force Gate.init to be called first
  @Resource
  private Init init

  /**
   * Normalise annotationSelectors and default set name to trim whitespace and remove empty
   * or null values.
   */
  @PostConstruct
  public void loadExtraMimePlugins() {
    if(gateElgProperties.extraPluginsApp) {
      log.info("Loading extra plugins via {}", gateElgProperties.extraPluginsApp)
      gate.Resource res = null
      try {
        // load the extraPluginsApp for its side effects
        res = PersistenceManager.loadObjectFromUrl(SpringFactory.resourceToUrl(gateElgProperties.extraPluginsApp))
      } catch(Exception e) {
        log.warn("Unable to load extraPluginsApp from ${gateElgProperties.extraPluginsApp}", e)
      }
      if(res) {
        // ... and delete it again
        Factory.deleteResource(res)
      }
    }

    // apply MIME type alias mappings, if any
    if(gateElgProperties.mimeAliases) {
      MimeTypeMapper.applyMimeMappings(gateElgProperties.mimeAliases)
    }
  }


  /**
   * Apply mime type mappings according to the given mappings.  We treat
   * each key as an additional mime type A, the corresponding value should be
   * another mime type B, and we update the internal DocumentFormat maps so
   * type A is linked to the same format handler as type B.
   */
  public static void applyMimeMappings(Map<String, String> mappings) {
    mappings.each { sourceType, targetType ->
      log.info("Treating {} as {}", sourceType, targetType)
      def targetHandler = mimeString2ClassHandlerMap.get(targetType)
      if(targetHandler) {
        def majorMinor = sourceType.split('/', 2)
        if(majorMinor.size() >= 2) {
          def mime = new MimeType(majorMinor[0], majorMinor[1])
          mimeString2mimeTypeMap.put(sourceType, mime)
          mimeString2ClassHandlerMap.put(sourceType, targetHandler)
        }
      } else {
        log.warn("No document format found for {} - mapping ignored", targetType)
      }
    }
  }

  // no-ops to satisfy the compiler, these will never be called
  public void unpackMarkup(Document d) {}
  public void unpackMarkup(Document d, RepositioningInfo repos, RepositioningInfo ampCoding) {}
}
