/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.util

import gate.elg.config.GateElgProperties
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import gate.Corpus;
import gate.Document
import gate.Factory
import gate.LanguageAnalyser;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * DocumentProcessor implementation that exits the host JVM if the analyser takes longer than a configurable timeout.
 */
@Slf4j
public class ExitOnTimeoutDocumentProcessor implements DocumentProcessor {

  LanguageAnalyser analyser

  private static AtomicInteger idSource = new AtomicInteger(0)

  private int myId = idSource.getAndIncrement()
  
  Corpus corpus

  @Autowired
  GateElgProperties gateElgProperties

  private int interruptTimeout

  private int killTimeout

  private Timer killTimer = new Timer(true)
  
  @Override
  public void processDocument(Document doc) throws GateException {
    log.trace("Processing document {} with processor {}", doc.name, myId)
    // clear interruption status
    Thread.interrupted()
    
    Thread processThread = Thread.currentThread()
    TimerTask interruptTask = killTimer.runAfter(interruptTimeout) {
      log.warn("Task running for ${interruptTimeout} ms, attempting to interrupt")
      analyser.interrupt()
      processThread.interrupt()
    }
    TimerTask killTask = killTimer.runAfter(killTimeout) {
      log.error("Task took longer than ${killTimeout} ms, shutting down")
      System.exit(0)
    }
    corpus.add(doc)
    analyser.setDocument(doc)
    try {
      analyser.execute()
    } finally {
      killTask.cancel()
      interruptTask.cancel()
      // make sure we clear interrupt status again
      Thread.interrupted()
      analyser.setDocument(null)
      corpus.clear()
    }
  }
  
  @PostConstruct
  public synchronized void init() {
    log.trace("Creating new processor with id {}", myId)
    interruptTimeout = (int)gateElgProperties.interruptTimeout.toMillis()
    killTimeout = (int)gateElgProperties.killTimeout.toMillis()
    corpus = Factory.newCorpus("DocumentProcessor corpus")
    log.trace("Created corpus {}", corpus.getName())
    analyser.setCorpus(corpus)
    // if (as it should be) the analyser is a controller that
    // supports callbacks, invoke them now
    if(analyser.respondsTo("setControllerCallbacksEnabled")) {
      try {
        analyser.setControllerCallbacksEnabled(false)
      } catch(Throwable t) {
        log.warn("Error calling setControllerCallbacksEnabled", t)
      }
    }
    if(analyser.respondsTo("invokeControllerExecutionStarted")) {
      try {
        analyser.invokeControllerExecutionStarted()
      } catch(Throwable t) {
        log.warn("Error calling invokeControllerExecutionStarted", t)
      }
    }
  }
  
  @PreDestroy
  public synchronized void cleanup() {
    log.trace("Cleaning up processor {}", myId)
    killTimer.cancel()
    if(analyser.respondsTo("invokeControllerExecutionFinished")) {
      try {
        analyser.invokeControllerExecutionFinished()
      } catch(Throwable t) {
        log.warn("Error invoking controllerExecutionFinished", t)
      }
    }
    Factory.deleteResource(analyser)
    log.trace("Deleted analyser")
    if(corpus) {
      Factory.deleteResource(corpus)
      log.trace("Deleted corpus")
    }
  }
}
