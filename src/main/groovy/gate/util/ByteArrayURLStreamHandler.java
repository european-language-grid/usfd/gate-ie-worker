/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.util;

import org.springframework.util.FastByteArrayOutputStream;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/**
 * This oddity is just a wrapper around a byte array and a URL, to allow creation of GATE documents from a fully-written
 * {@link FastByteArrayOutputStream} with application/pdf type.  Adapted from https://gate.ac.uk/gcp
 */
public class ByteArrayURLStreamHandler
        extends URLStreamHandler {

  private FastByteArrayOutputStream data;

  public ByteArrayURLStreamHandler(FastByteArrayOutputStream data) {
    this.data = data;
  }

  public URLConnection openConnection(URL u) {
    return new URLConnection(u) {
      public void connect() {
        // do nothing, but superclass method is abstract
      }

      public InputStream getInputStream() {
        return data.getInputStream();
      }

    };
  }


}
