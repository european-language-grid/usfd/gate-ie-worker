/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.web

import eu.elg.handler.HandlerException
import eu.elg.model.Failure
import eu.elg.model.Response
import eu.elg.model.StandardMessages
import eu.elg.model.StatusMessage
import eu.elg.model.requests.TextRequest
import gate.elg.config.GateElgProperties
import gate.elg.process.GateProcessor
import gate.util.DocumentProcessor
import groovy.util.logging.Slf4j
import org.codehaus.groovy.runtime.InvokerInvocationException
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.util.FastByteArrayOutputStream
import org.springframework.web.reactive.function.BodyExtractors
import org.springframework.web.reactive.function.server.HandlerFunction
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import reactor.pool.Pool
import reactor.pool.PooledRef

import java.lang.reflect.InvocationTargetException
import java.lang.reflect.UndeclaredThrowableException

/**
 * REST controller that accepts anything GATE can understand over HTTP and responds with the ELG-style response - useful
 * for testing and integration with non-ELG platforms.
 */
@Slf4j
class RestProcessor implements HandlerFunction<ServerResponse> {

  public RestProcessor(GateProcessor gateProcessor, Pool<DocumentProcessor> processorPool, GateElgProperties.Service service) {
    this.gateProcessor = gateProcessor;
    this.processorPool = processorPool;
    this.service = service;
  }

  GateProcessor gateProcessor

  Pool<DocumentProcessor> processorPool

  GateElgProperties.Service service

  /**
   * Version that consumes anything GATE can handle and assumes it is just the plain content.
   */
  Mono<ServerResponse> handle(ServerRequest serverRequest) {
    TextRequest request = new TextRequest().withMimeType(
            serverRequest.headers().contentType().orElse(MediaType.TEXT_PLAIN).toString())
            .withParams([:])
    def annotations = serverRequest.queryParams()?.get("annotations")
    if(annotations) {
      request.params.annotations = (annotations.join(",").trim().split(/\s*,\s*/).toList())
    }
    def includeText = serverRequest.queryParams()?.getFirst("includeText")
    if(includeText?.equalsIgnoreCase("true") || includeText?.equalsIgnoreCase("yes")) {
      request.params.includeText = includeText
    }

    Mono.usingWhen(
            processorPool.acquire(),
            { PooledRef<DocumentProcessor> processorRef ->
              DocumentProcessor processor = processorRef.poolable();
              if(processor == null) {
                return Mono.error(new HandlerException(StandardMessages.elgRequestMissing()));
              }

              // get the content
              serverRequest.body(BodyExtractors.toDataBuffers()).publishOn(Schedulers.boundedElastic()).collect(
                      { -> new FastByteArrayOutputStream() },
                      { FastByteArrayOutputStream stream, buf ->
                        buf.asInputStream(true).withStream { stream << it }
                      }).map({ FastByteArrayOutputStream stream ->
                gateProcessor.process(service, processor, request, stream)
              })
            },
            { ref -> ref.release() }
    ).flatMap({ Response<?> response ->
      ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).bodyValue(response.asMessage())
    }).onErrorResume({ throwable ->
      StatusMessage[] messages = null
      // unwrap common wrapper exception types
      if(throwable instanceof UndeclaredThrowableException || throwable instanceof InvocationTargetException || throwable instanceof InvokerInvocationException) {
        throwable = throwable.getCause()
      }
      if(throwable instanceof HandlerException) {
        messages = throwable.status
      } else {
        StatusMessage msg = StandardMessages.elgServiceInternalError(throwable.message)
        if(log.traceEnabled) {
          msg.withStackTrace(throwable)
        }
        messages = [msg] as StatusMessage[]
      }
      log.error("Error during processing", throwable)
      ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON).bodyValue(
              new Failure().withErrors(messages).asMessage())
    })

  }
}
