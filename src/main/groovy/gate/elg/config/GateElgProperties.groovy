/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.config


import groovy.util.logging.Slf4j
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.core.io.Resource
import org.springframework.stereotype.Component

import java.time.Duration

/**
 * Strongly typed configuration properties for the GATE worker.
 */
@Slf4j
@Component
@ConfigurationProperties(prefix = "gate.elg")
class GateElgProperties {

  static class Service {
    /**
     * Path at which this service will be exposed.
     */
    String path = "/process"

    /**
     * Location of the main saved application with which the text requests will be processed.
     */
    Resource gappLocation

    /**
     * Maximum number of concurrent requests that can be served by this service (i.e. max pool size)
     */
    int concurrency = 1

    /**
     * Default annotation set name, used for selectors that don't contain a colon.
     */
    String defaultSet = ""

    /**
     * Annotation selectors in the usual set:type format.  Any selectors that do not include a colon will be assumed to
     * refer to the {@link #defaultSet}.
     */
    List<String> annotationSelectors = []
  }

  List<Service> services


  /**
   * Location of an extra application that is loaded once at startup purely for its side effects (e.g. loading
   * document format plugins)
   */
  Resource extraPluginsApp

  /**
   * Declare additional MIME types that this tool should support.  Each map entry has as the key a MIME type we
   * expect to receive from a caller, and as the value the standard GATE MIME type to which this should be an alias.
   * In application.yml this must be specified with square brackets for the keys, as Spring munges map keys that
   * contain characters like forward slash:
   *
   * <pre>
   *   gate:
   *     elg:
   *       mimeAliases:
   *         "[application/json]": "text/x-json-twitter"
   * </pre>
   */
  Map<String, String> mimeAliases

  /**
   * Time to wait from the start of processing before interrupting the running application
   */
  Duration interruptTimeout

  /**
   * Time to wait from the start of processing before exiting this JVM - must be longer than {@link #interruptTimeout}
   */
  Duration killTimeout

}
