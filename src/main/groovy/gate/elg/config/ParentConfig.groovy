/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.config;

import gate.util.MimeTypeMapper
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ImportResource

/**
 * Top-level configuration class for the parent context, which will only contain GateElgProperties (via component scan)
 * and the MIME type mapper
 */
@SpringBootApplication
@ImportResource(["classpath:spring/init-gate.xml"])
public class ParentConfig {
  @Bean
  public MimeTypeMapper mimeTypeMapper(GateElgProperties gateElgProperties) {
    new MimeTypeMapper(gateElgProperties: gateElgProperties)
  }


}
