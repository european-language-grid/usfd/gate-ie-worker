/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.config;

import gate.util.DocumentProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.pool.Pool;
import reactor.pool.PoolBuilder;

public class ProcessorPoolFactoryBean implements FactoryBean<Pool<DocumentProcessor>>, BeanFactoryAware {

  private static final Logger log = LoggerFactory.getLogger(ProcessorPoolFactoryBean.class);

  private GateElgProperties.Service service;

  private ConfigurableBeanFactory beanFactory;

  public void setService(GateElgProperties.Service service) {
    this.service = service;
  }

  @Override
  public Pool<DocumentProcessor> getObject() throws Exception {
    String prototypeDocProcessor = "documentProcessor-" + service.getPath();
    return PoolBuilder.from(Mono.fromCallable(() -> {
      log.debug("Getting documentProcessor bean for path {} from context", service.getPath());
      return beanFactory.getBean(prototypeDocProcessor, DocumentProcessor.class);
    }).subscribeOn(Schedulers.boundedElastic()))
            .destroyHandler((docProc) ->
                    Mono.<Void>empty().doOnTerminate(() -> beanFactory.destroyBean(prototypeDocProcessor, docProc))
            )
            .sizeBetween(1, service.getConcurrency())
            .fifo();
  }

  @Override
  public Class<?> getObjectType() {
    return null;
  }

  public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
    this.beanFactory = (ConfigurableBeanFactory)beanFactory;
  }
}
