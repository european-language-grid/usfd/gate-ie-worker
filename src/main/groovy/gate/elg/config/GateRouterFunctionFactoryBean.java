/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.config;

import gate.DocumentFormat;
import gate.elg.web.RestProcessor;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

public class GateRouterFunctionFactoryBean implements FactoryBean {

  private RestProcessor restProcessor;

  private GateElgProperties.Service service;

  public void setRestProcessor(RestProcessor restProcessor) {
    this.restProcessor = restProcessor;
  }

  public void setService(GateElgProperties.Service service) {
    this.service = service;
  }

  @Override
  public Object getObject() throws Exception {
    return RouterFunctions.route()
            .POST("/gate" + service.getPath(),
                    RequestPredicates.contentType(
                            DocumentFormat.getSupportedMimeTypes().stream().map(MediaType::parseMediaType)
                                    .toArray(MediaType[]::new)),
                    restProcessor)
            .build()
    ;
  }

  @Override
  public Class<?> getObjectType() {
    return RouterFunction.class;
  }
}
