/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg


import gate.elg.config.ParentConfig
import gate.elg.process.WebConfig
import groovy.util.logging.Slf4j
import org.springframework.boot.builder.SpringApplicationBuilder

/**
 * Main Spring Boot application entry point.
 */
@Slf4j
class Application {
  public static void main(String[] args) {
    // we define a two-level application context - the parent context just defines the ConfigurationProperties,
    // the child context then uses these parsed properties to generate the actual handler beans.
    new SpringApplicationBuilder(ParentConfig).child(WebConfig).run(args)
  }
}
