/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.process

import eu.elg.handler.HandlerException
import eu.elg.model.Markup
import eu.elg.model.Response
import eu.elg.model.StandardMessages
import eu.elg.model.requests.TextRequest
import eu.elg.model.AnnotationObject
import eu.elg.model.responses.AnnotationsResponse
import eu.elg.model.responses.TextsResponse
import gate.*
import gate.corpora.RepositioningInfo
import gate.elg.config.GateElgProperties
import gate.util.ByteArrayURLStreamHandler
import gate.util.DocumentProcessor
import gate.util.GateException
import groovy.util.logging.Slf4j
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component
import org.springframework.util.FastByteArrayOutputStream

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Class that encapsulates the main logic, called from both the GateHandler and the RestProcessController.
 */
@Slf4j
@Component
@DependsOn("mimeTypeMapper")
class GateProcessor {

  /**
   * Process request and return a response immediately.
   *
   * @param request the request data
   * @param content content to process, if the request does not contain an inline "content" parameter
   * @return response with annotations
   * @throws GateException if an exception occurs during processing
   */
  public <R extends Response<R>> R process(GateElgProperties.Service service, DocumentProcessor documentProcessor, TextRequest request, FastByteArrayOutputStream content) throws GateException, HandlerException {
    FeatureMap docParams = Factory.newFeatureMap()
    if(request.content) {
      docParams.stringContent = request.content
      log.debug("Processing inline content ({} characters) with service {}", request.content.length(), service.path)
    } else if(content != null) {
      docParams.sourceUrl = new URL(null, "file:/dummy",
              new ByteArrayURLStreamHandler(content))
      log.debug("Processing out-of-band content ({} bytes) with service {}", content.size(), service.path)
    } else {
      throw new HandlerException(StandardMessages.elgRequestMissing())
    }
    if(request.mimeType) {
      docParams.mimeType = request.mimeType
      // attempt to extract an encoding from the mime type, assume UTF-8 if not specified
      String[] mimeAndParams = request.mimeType.split(';');
      // look for a charset parameter, if there is one
      for(int i = 1; i < mimeAndParams.length; i++) {
        String[] keyAndVal = mimeAndParams[i].split('=', 2)
        if(keyAndVal.length == 2 && keyAndVal[0].trim().equals('charset')) {
          docParams.encoding = keyAndVal[1].trim()
          break
        }
      }

      if(!docParams.encoding) {
        docParams.encoding = "UTF-8"
      }
    }

    FeatureMap docFeatures = null
    if(request.markup?.features) {
      docFeatures = Utils.toFeatureMap(request.markup?.features)
    }
    Document gateDoc = (Document)Factory.createResource("gate.corpora.DocumentImpl", docParams, docFeatures)
    try {
      if(request.markup?.annotations) {
        // add annotations from the request to the document
        String initialText = gateDoc.content.toString()
        RepositioningInfo inRepos = repositionForSupplementaries(initialText)
        request.markup.annotations.each { annType, anns ->
          // parse the annotation type - if it contains a colon then assume it's setName:type, if it doesn't then
          // use the default set for this service
          String[] setAndType = annType.split(':', 2)
          AnnotationSet annotationSet
          String typeToCreate
          if(setAndType.length == 1) {
            // only the annotation type
            annotationSet = gateDoc.getAnnotations(service.defaultSet)
            typeToCreate = setAndType[0]
          } else {
            annotationSet = gateDoc.getAnnotations(setAndType[0])
            typeToCreate = setAndType[1]
          }
          anns.each { ann ->
            annotationSet.add(inRepos.getExtractedPos(ann.start.longValue()),
                    inRepos.getExtractedPos(ann.end.longValue()),
                    typeToCreate, Utils.toFeatureMap(ann.features))
          }
        }
      }

      // process the document
      documentProcessor.processDocument(gateDoc)
      // we can't assume the text and repositioning is still the same as it was - app may have modified the text
      String docText = gateDoc.content.toString()
      RepositioningInfo repos = repositionForSupplementaries(docText)
      def selectors = (request.params?.annotations) ? [request.params.annotations].flatten() : service.annotationSelectors
      def returnAnnotations = selectors.collectEntries { String sel ->
        String[] setAndType = sel.split(':', 2)
        AnnotationSet annotationSet
        if(setAndType.length == 1) {
          // only the annotation type
          annotationSet = gateDoc.getAnnotations(service.defaultSet).get(setAndType[0])
        } else {
          annotationSet = gateDoc.getAnnotations(setAndType[0]).get(setAndType[1])
        }

        return [sel, Utils.inDocumentOrder(annotationSet).collect { Annotation a ->
          new AnnotationObject(start: repos.getOriginalPos(Utils.start(a), false),
                  end: repos.getOriginalPos(Utils.end(a), true), features: a.features)
        }]
      }
      if(request.params?.includeText) {
        return new TextsResponse().withTexts(new TextsResponse.Text().withContent(docText)
                .withMarkup(new Markup().withAnnotations(returnAnnotations)))
      } else {
        return new AnnotationsResponse().withAnnotations(returnAnnotations)
      }
    } finally {
      Factory.deleteResource(gateDoc)
    }
  }

  private static final Pattern CHARS_TO_ESCAPE = Pattern.compile("[\\x{" +
          Integer.toHexString(Character.MIN_SUPPLEMENTARY_CODE_POINT) + "}-\\x{" +
          Integer.toHexString(Character.MAX_CODE_POINT) + "}]");


  /**
   * GATE deals with annotation offsets in the same way as standard Java string indices, as UTF-16 code units.
   * This method calculates the adjustments necessary to shift the annotation offsets into terms of Unicode code
   * points if the document contains supplementary characters such as Emoji.
   * @param text the document text, which may contain supplementary characters.
   * @return GATE RepositioningInfo representing the adjustments
   */
  RepositioningInfo repositionForSupplementaries(String text) {
    int origOffset = 0;
    int extractedOffset = 0;
    RepositioningInfo repos = new RepositioningInfo()
    Matcher mat = CHARS_TO_ESCAPE.matcher(text);
    while(mat.find()) {
      if(mat.start() != extractedOffset) {
        // repositioning record for the span from end of previous match to start of this one
        int nonMatchLen = mat.start() - extractedOffset;
        repos.addPositionInfo(origOffset, nonMatchLen, extractedOffset, nonMatchLen);
        origOffset += nonMatchLen;
        extractedOffset += nonMatchLen;
      }

      // the extracted length is the number of code units matched by the pattern
      // (should always be 2 for the two halves of the surrogate pair)
      int extractedLen = mat.end() - mat.start();

      // repositioning record covering this match
      repos.addPositionInfo(origOffset, 1, extractedOffset, extractedLen);
      origOffset += 1;
      extractedOffset += extractedLen;
    }
    int tailLen = text.length() - extractedOffset;
    // repositioning record covering everything after the last match - not strictly
    // required on GATE 8.6+ if tailLen is zero but won't do any harm in that case
    // and works around a bug in earlie versions
    repos.addPositionInfo(origOffset, tailLen + 1, extractedOffset, tailLen + 1);

    return repos
  }
}
