/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package gate.elg.process;

import eu.elg.handler.*;
import eu.elg.model.Response;
import eu.elg.model.StandardMessages;
import eu.elg.model.requests.TextRequest;
import gate.elg.config.GateElgProperties;
import gate.util.DocumentProcessor;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.pool.Pool;
import reactor.pool.PooledRef;

/**
 * ELG handler for GATE IE text requests.
 */
public class GateHandler implements Handler.Async.General<TextRequest>, ElgHandlerRegistration {

  public GateHandler(GateProcessor gateProcessor, Pool<DocumentProcessor> processorPool, GateElgProperties.Service service) {
    this.gateProcessor = gateProcessor;
    this.processorPool = processorPool;
    this.service = service;
  }

  private GateProcessor gateProcessor;

  private Pool<DocumentProcessor> processorPool;

  private GateElgProperties.Service service;

  public Mono<Response<?>> handle(final TextRequest request) {
    return Mono.usingWhen(
            processorPool.acquire(),
            (PooledRef<DocumentProcessor> processorRef) -> {
              DocumentProcessor processor = processorRef.poolable();
              if(processor == null) {
                return Mono.error(new HandlerException(StandardMessages.elgRequestMissing()));
              }
              return Mono.<Response<?>>fromCallable(() -> {
                return gateProcessor.process(service, processor, request, null);
              }).subscribeOn(Schedulers.boundedElastic());
            },
            PooledRef::release
    );
  }

  @Override
  public void registerHandlers(ElgHandlerRegistrar registrar) {
    registrar.asyncHandler(service.getPath(), TextRequest.class, this);
  }
}
