/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package spring

import gate.elg.config.GateElgProperties
import gate.elg.config.GateRouterFunctionFactoryBean
import gate.elg.config.ProcessorPoolFactoryBean
import gate.elg.process.GateHandler
import gate.elg.web.RestProcessor
import gate.util.ExitOnTimeoutDocumentProcessor
import gate.util.spring.DuplicateResourceFactoryBean
import gate.util.spring.SavedApplicationFactoryBean
import groovy.transform.Field
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.support.AutowireCandidateQualifier

@Field Logger log = LoggerFactory.getLogger("gate.elg.config.spring.resources")

beans {

  // gateElgProperties comes from the parent context
  List<GateElgProperties.Service> services = registry.getBean('gateElgProperties').services
  for(service in services) {
    log.debug("Defining beans for path {}", service.path)
    // template Controller loaded from gappLocation
    "plainGateApp-${service.path}"(SavedApplicationFactoryBean) { bean ->
      bean.autowireCandidate = false
      location = service.gappLocation
    }

    // duplicator
    "gateApp-${service.path}"(DuplicateResourceFactoryBean) { bean ->
      template = ref("plainGateApp-${service.path}")
      returnTemplate = false
    }

    // prototype DocumentProcessor which the pool will reference by name
    "documentProcessor-${service.path}"(ExitOnTimeoutDocumentProcessor) { bean ->
      bean.scope = "prototype"
      analyser = ref("gateApp-${service.path}")
    }

    // reactor pool based on that DocumentProcessor definition
    "processorPool-${service.path}"(ProcessorPoolFactoryBean) {
      delegate.service = service
    }

    // ELG handler
    "elgHandler-${service.path}"(GateHandler, ref('gateProcessor'), ref("processorPool-${service.path}"), service) { bean ->
      bean.getBeanDefinition().addQualifier(new AutowireCandidateQualifier(Qualifier, "ElgHandler"))
    }

    // GATE-specific endpoint
    "restProcessor-${service.path}"(RestProcessor, ref('gateProcessor'), ref("processorPool-${service.path}"), service)

    // router function for the GATE endpoint
    "routerFunction-${service.path}"(GateRouterFunctionFactoryBean) { bean ->
      bean.dependsOn = 'mimeTypeMapper'
      restProcessor = ref("restProcessor-${service.path}")
      delegate.service = service
    }
  }

}