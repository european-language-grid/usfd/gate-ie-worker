# syntax = docker/dockerfile:1.4
#
#
# Very simple Docker base image for GATE-based ELG IE services.  To use, derive
# a child image FROM this one and add the GATE app as
# /application/application.xgapp owned by the "elg" user and group - the
# easiest way to achieve this is to "export for GATE Cloud" and then unpack the
# resulting ZIP file into /application in the image.  You can (and should) also
# add /elg/application.yml with the following entries:
#
# elg:
#   # annotation types your app will return by default
#   annotationSelectors: Person,Location
#   # if your app puts its annotations into a set other than the default one
#   defaultSet: Output
#
# If you need to return annotations from more than one set you can do so by
# using "set:type" notation, e.g. annotationSelectors: "Person,Automatic:Term"
#
FROM azul/zulu-openjdk-debian:8-latest

ARG TARGETARCH

ADD https://github.com/krallin/tini/releases/download/v0.19.0/tini-$TARGETARCH /sbin/tini
RUN chmod +x /sbin/tini && \
      addgroup --gid 1001 "elg" && \
      adduser --disabled-password --gecos "ELG User,,," \
      --home /elg --ingroup elg --no-create-home --uid 1001 elg

ARG JAR_VERSION
COPY --chown=1001:1001 build/libs/gate-ie-worker-$JAR_VERSION.jar /elg/
COPY --chown=1001:1001 entrypoint.sh /elg/
COPY --chown=1001:1001 build/gate-plugins /elg/gate-plugins

USER 1001:1001

WORKDIR /elg

ENTRYPOINT ["/sbin/tini", "-e", "143", "--", "/elg/entrypoint.sh"]
CMD ["--spring.profiles.active=docker,gate-endpoint"]
